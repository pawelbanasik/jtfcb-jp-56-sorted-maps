import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Main {

	public static void main(String[] args) {

		// hashMap nie ma uporzadkowanych element�w
		//HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
		// lub taki zapis przez interfejs Map
		// robi se kolejnosc sam czyli nie tak jak wpiszesz
		Map<Integer, String> hashMap = new HashMap<Integer, String>();
		
		
		// jak chcesz uporzadkowane
		// to nadaje sie linkedHashMap
		// sa w tej samej kolejnosci jak je dodajesz
		//LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<Integer, String>();
		// lub taki zapis przez interfejs Map
		// zachowuje kolejnosc
		Map<Integer, String> linkedHashMap = new LinkedHashMap<Integer, String>();
		
		
		//TreeMap<Integer, String> linkedTreeMap = new TreeMap<Integer, String>();
		// lub taki zapis przez interfejs Map
		// drzewo sortuje klucze
		Map<Integer, String> linkedTreeMap = new TreeMap<Integer, String>();
		
		testMap(hashMap);
		testMap(linkedHashMap);
		testMap(linkedTreeMap);
	}

	public static void testMap(Map<Integer, String> map) {

		map.put(9, "fox");
		map.put(4, "cat");
		map.put(8, "dog");
		map.put(1, "giraffe");
		map.put(0, "swan");
		map.put(15, "bear");
		map.put(6, "snake");

		// 2 sposob na iteracje, 1 sposb byl w poprzednim filmiku
		for (Integer key : map.keySet()) {

			String value = map.get(key);
			System.out.println(key + ":" + value);
		}

	}
}